using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LibraryMenuScene : Microsoft.Xna.Framework.DrawableGameComponent
    {
        Game baseGame;
        int frame;
        int frameCount;
        int delay;
        Vector2 location;
        Rectangle hoverArea;
        SpriteBatch spriteBatch;
        Texture2D pan;
        Rectangle[] panRects;
        bool hover;
        Point pos;
        Texture2D theaterBkg;
        Vector2 theaterBkgPos;

        public LibraryMenuScene(Game game, float X, float Y)
            : base(game)
        {
            // TODO: Construct any child components here
            baseGame = game;
            location = new Vector2(X, Y);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            frame = 0;
            frameCount = 31;
            panRects = new Rectangle[31];
            for (int i = 0; i < frameCount; i++)
                panRects[i] = new Rectangle(i%9 * 453, (int)i/9 * 115, 453, 115);
            hoverArea = new Rectangle((int)location.X, (int)location.Y, 453, 115);
            pos = new Point(Mouse.GetState().X, Mouse.GetState().Y);
            theaterBkgPos = new Vector2(855f, 260f);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            theaterBkg = baseGame.Content.Load<Texture2D>(@"images/StageBackground");
            pan = baseGame.Content.Load<Texture2D>("images/TheaterPanWalking");
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            pos = new Point(Mouse.GetState().X, Mouse.GetState().Y);
            hover = hoverArea.Contains(pos);

            if (!hover)
                frame = 0;

            // TODO: Add your update code here
            if (frame != 23)
                frame++;
            else
            {
                delay--;
                if (delay < 1)
                {
                    delay = 120;
                    frame++;
                }
            }

            if(frame > 30) frame = 0;
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch = baseGame.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(theaterBkg, theaterBkgPos, Color.White);
            if(hover) spriteBatch.Draw(pan, location, panRects[frame], Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
