﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Epic_Elementary
{
    public class Projectile
    {
        public Vector3 position, size, origin, destination;
        public Texture2D texture;
        public bool isPlayer, active;
        public float timeout;
        public int vVelocity;
        public float scale;
        public BoundingBox bb;
        public double type;

        public Projectile(Vector3 origin, Vector3 dest, bool player, Vector2 dimensions, double rand)
        {
            size = new Vector3(dimensions.X, dimensions.Y, 20);
            active = true;
            isPlayer = player;
            vVelocity = 15;
            send(origin, dest);
            type = rand;
            if (type < .66)
                scale = 1f;
            else 
                scale = .5f;
        }

        public void send(Vector3 origin, Vector3 destination)
        {
            this.position = origin;
            this.origin = origin;
            this.destination = destination;
            adjustboundingbox();
        }

        public void move()
        {
            this.vVelocity -= 1;
            this.position.Y += vVelocity;
            this.position.X += (this.destination.X - this.origin.X) / 30;
            this.position.Z += (this.destination.Z - this.origin.Z) / 30;
            adjustboundingbox();
        }

        public Vector2 displayCoords()
        {
            return new Vector2(this.position.X, this.position.Z+this.position.Y*-1);
        }

        public void adjustboundingbox()
        {
            this.bb = new BoundingBox(new Vector3(this.position.X, this.position.Y, this.position.Z), new Vector3(this.position.X + this.size.X * this.scale, this.position.Y + (this.size.Y *this.scale), this.position.Z + this.size.Z));
        }

    }
}
