using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Render : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Render(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        Texture2D healthbar;

        Enemies enemy;

        Player player;

        Projectiles projectile;
        
        SpriteBatch batch;
        Window window;

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            healthbar = Game.Content.Load<Texture2D>(@"images/dummy");
            window = Game.Services.GetService(typeof(Window)) as Window;
            player = Game.Services.GetService(typeof(Player)) as Player;
            enemy = Game.Services.GetService(typeof(Enemies)) as Enemies;
            projectile = Game.Services.GetService(typeof(Projectiles)) as Projectiles;
            base.Initialize();
        }

        protected override void LoadContent()
        {

            player.actor.spriteTexture = Game.Content.Load<Texture2D>(@"images/player/jack");

            base.LoadContent();

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }



        public override void Draw(GameTime gameTime)
        {
            batch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;

            base.Draw(gameTime);

            float width = (window.view.X/4)/healthbar.Width;

            batch.Begin(SpriteSortMode.BackToFront, null, SamplerState.LinearWrap, null, null, null, Matrix.CreateTranslation(window.location * -1));

            batch.Draw(
                healthbar,
                new Vector2(window.location.X + window.view.X/32 , window.view.Y/32),
                null,
                Color.Red,
                0f,
                new Vector2(0f, 0f),
                new Vector2(width, (window.view.Y/16)/healthbar.Height),
                SpriteEffects.None,
                0
                );
            batch.Draw(
                healthbar,
                new Vector2(window.location.X + window.view.X / 32, window.view.Y / 32),
                null,
                Color.Green,
                0f,
                new Vector2(0f, 0f),
                new Vector2(width * ((float)player.actor.health/100), (window.view.Y / 16) / healthbar.Height),
                SpriteEffects.None,
                0
                );
            
            if (player.animate.flipped)
            {
                batch.Draw(
                    player.actor.spriteTexture,
                    player.actor.displayCoordinates(),
                    player.frame,
                    Color.White,
                    0f,
                    new Vector2(0f, 0f),
                    player.animate.scale,
                    SpriteEffects.FlipHorizontally,
                    (1 / player.actor.position.Z)
                );
            }
            else
            {
                batch.Draw(
                    player.actor.spriteTexture,
                    player.actor.displayCoordinates(),
                    player.frame,
                    Color.White,
                    0f,
                    new Vector2(0f, 0f),
                    player.animate.scale,
                    SpriteEffects.None,
                    (1 / player.actor.position.Z)
                );
            }

            for (int i = 0; i < enemy.enemies.Count; i++)
            {
                if (enemy.animations[i].flipped)
                {
                    batch.Draw(
                        enemy.enemies[i].spriteTexture,
                        enemy.enemies[i].displayCoordinates(),
                        enemy.animations[i].animation(enemy.enemies[i].momentum, gameTime),
                        Color.White,
                        0f,
                        new Vector2(0f, 0f),
                        enemy.animations[i].scale,
                        SpriteEffects.FlipHorizontally,
                        (1 / enemy.enemies[i].position.Z)); 
                }
                else
                {
                    batch.Draw(
                        enemy.enemies[i].spriteTexture,
                        enemy.enemies[i].displayCoordinates(),
                        enemy.animations[i].animation(enemy.enemies[i].momentum, gameTime),
                        Color.White,
                        0f,
                        new Vector2(0f, 0f),
                        enemy.animations[i].scale,
                        SpriteEffects.None,
                        (1 / enemy.enemies[i].position.Z));
                }
            }
            if (projectile.projectiles.Count != 0)
            {
                for (int i = 0; i < projectile.projectiles.Count; i++)
                {
                    if (projectile.projectiles[i].type < .33)
                        batch.Draw(
                            projectile.texture2,
                            projectile.projectiles[i].displayCoords(),
                            null,
                            Color.White,
                            0f,
                            new Vector2(0f,0f),
                            projectile.projectiles[i].scale,
                            SpriteEffects.None,
                            (1/projectile.projectiles[i].position.Z)
                        );
                    else if (projectile.projectiles[i].type < .66)
                        batch.Draw(
                            projectile.texture3,
                            projectile.projectiles[i].displayCoords(),
                            null,
                            Color.White,
                            0f,
                            new Vector2(0f, 0f),
                            projectile.projectiles[i].scale,
                            SpriteEffects.None,
                            (1 / projectile.projectiles[i].position.Z)
                        );
                    else
                        batch.Draw(
                            projectile.texture,
                            projectile.projectiles[i].displayCoords(),
                            null,
                            Color.White,
                            0f,
                            new Vector2(0f, 0f),
                            projectile.projectiles[i].scale,
                            SpriteEffects.None,
                            (1 / projectile.projectiles[i].position.Z)
                        );
                }
            }

            batch.End();

            base.Update(gameTime);
        }
    }
}
