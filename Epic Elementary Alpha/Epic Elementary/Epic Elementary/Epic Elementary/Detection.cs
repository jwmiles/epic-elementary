using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Detection : Microsoft.Xna.Framework.GameComponent
    {
        public Detection(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        Player player;
        Enemies enemies;
        Projectiles projectiles;

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            player = Game.Services.GetService(typeof(Player)) as Player;
            enemies = Game.Services.GetService(typeof(Enemies)) as Enemies;
            projectiles = Game.Services.GetService(typeof(Projectiles)) as Projectiles;

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            for (int i = 0; i < projectiles.projectiles.Count; i++)
            {
                if (projectiles.projectiles[i].isPlayer)
                {
                    for (int j = 0; j < enemies.enemies.Count; j++)
                    {
                        if (projectiles.projectiles[i].bb.Intersects(enemies.enemies[j].boundingbox))
                        {
                            projectiles.projectiles.RemoveAt(i);
                            i--;
                            enemies.enemies[j].health -= 30;
                            if (enemies.enemies[j].health <= 0)
                            {
                                enemies.enemies.RemoveAt(j);
                                j--;
                            }
                            if (i < 0) i = 0;
                        }
                    }
                }
                else
                {
                    if (projectiles.projectiles[i].bb.Intersects(player.actor.boundingbox))
                    {
                        projectiles.projectiles.RemoveAt(i);
                        i--;
                        player.actor.health -= 5;

                        if (player.actor.health <= 0)
                        {
                        }
                    }
                }
            }
            base.Update(gameTime);
        }
    }
}
