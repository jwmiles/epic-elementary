using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Projectiles : Microsoft.Xna.Framework.GameComponent
    {
        float time = 0f;
        Player player;
        Level level;
        public Texture2D texture, texture2, texture3;
        public List<Projectile> projectiles = new List<Projectile>();
        Random r;

        public Projectiles(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            player = Game.Services.GetService(typeof(Player)) as Player;
            texture = Game.Content.Load<Texture2D>(@"images/projectiles/potato");
            texture2 = Game.Content.Load<Texture2D>(@"images/projectiles/ChickenLeg");
            texture3 = Game.Content.Load<Texture2D>(@"images/projectiles/Pizza");
            level = Game.Services.GetService(typeof(Level)) as Level;
            r = new Random();
            base.Initialize();
        }

        public void toss(Vector3 origin, Vector3 destination, bool isPlayer = false)
        {
            
            double distance = Math.Sqrt(Math.Pow((destination.X - origin.X), 2) + Math.Pow((destination.Z - origin.Z), 2));
            double slope = ((destination.Z - origin.Z) / (destination.X - origin.X));
            double y = origin.Z - (slope * origin.X);
            int negate = destination.X - origin.X > 0 ? 1 : -1;

            if (isPlayer)
            {
                if (distance > player.actor.maxRange)
                {
                    destination.Z = (float)slope * (origin.X + player.actor.maxRange * negate) + (float)y;
                    
                    destination.X = (destination.Z - (float)y) / (float)slope;
                    
                }
                else if (distance < player.actor.minRange)
                {
                    return;
                }
                if (destination.Z <= level.r_platform.Top)
                {
                    destination.Z = level.r_platform.Top;
                }
                else if (destination.Z >= level.r_platform.Bottom)
                {
                    destination.Z = level.r_platform.Bottom;
                }
            }

            this.projectiles.Add(new Projectile(origin, destination, isPlayer, new Vector2(texture.Width, texture.Height), r.NextDouble()));
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            time += gameTime.ElapsedGameTime.Milliseconds;

            for (int i = 0; i < projectiles.Count; i++)
            {
                if (projectiles[i].active == true)
                {
                    projectiles[i].move();
                    if (projectiles[i].position.Y <= 0)
                    {
                        projectiles[i].position.Y = 0;
                        projectiles[i].active = false;
                        projectiles[i].timeout = time + 2000;
                    }
                }
                else
                {
                    if (time > projectiles[i].timeout)
                    {
                        projectiles.RemoveAt(i);
                        i--;
                    }
                }
            }

            base.Update(gameTime);
        }
    }
}
