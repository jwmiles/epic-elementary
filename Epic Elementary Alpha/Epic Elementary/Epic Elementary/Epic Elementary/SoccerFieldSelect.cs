using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class SoccerFieldSelect : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Game baseGame;
        //Point primary, secondary;
        bool hover;
        Texture2D select;
        Point pos;
        Rectangle area;
        Enum State, gs;

        public SoccerFieldSelect(Game game, Enum gs)
            : base(game)
        {
            baseGame = game;
            this.gs = gs;
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            //primary = new Point(0, 415);
            //secondary = new Point(220, 0);
            area = new Rectangle(0, 0, 220, 415);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            select = baseGame.Content.Load<Texture2D>("images/SoccerFieldSelect");
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            pos = new Point(Mouse.GetState().X, Mouse.GetState().Y);

            //hover = this.Contains(primary, secondary, pos);
            hover = area.Contains(pos);

            if(hover)
            {
                if (Mouse.GetState().LeftButton.Equals(ButtonState.Pressed))
                    loadSoccerField();
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            spriteBatch = baseGame.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;
            spriteBatch.Begin();
            if(hover) spriteBatch.Draw(select, Vector2.Zero, Color.White);
            spriteBatch.End();
        }

        public void loadSoccerField()
        {
            /*
            //baseGame -- is the game file
            baseGame.Components.Clear();

            DrawableGameComponent mLevel = new Level(baseGame);
            baseGame.Components.Add(mLevel = new Level(baseGame));
            baseGame.Services.AddService(typeof(Level), mLevel);

            GameComponent mPlayer = new Player(baseGame);
            baseGame.Components.Add(mPlayer = new Player(baseGame));
            baseGame.Services.AddService(typeof(Player), mPlayer);

            DrawableGameComponent mWindow = new Window(baseGame);
            baseGame.Components.Add(mWindow = new Window(baseGame));
            baseGame.Services.AddService(typeof(Window), mWindow);

            GameComponent mController = new Controller(baseGame);
            baseGame.Components.Add(mController = new Controller(baseGame));
            baseGame.Services.AddService(typeof(Controller), mController);

            GameComponent mEnemies = new Enemies(baseGame);
            baseGame.Components.Add(mEnemies = new Enemies(baseGame));
            baseGame.Services.AddService(typeof(Enemies), mEnemies);

            GameComponent mAI = new AI(baseGame);
            baseGame.Components.Add(mAI = new AI(baseGame));
            baseGame.Services.AddService(typeof(AI), mAI);

            GameComponent mProjectiles = new Projectiles(baseGame);
            baseGame.Components.Add(mProjectiles = new Projectiles(baseGame));
            baseGame.Services.AddService(typeof(Projectiles), mProjectiles);


            GameComponent mDetection = new Detection(baseGame);
            baseGame.Components.Add(mDetection = new Detection(baseGame));
            baseGame.Services.AddService(typeof(Detection), mDetection);

            DrawableGameComponent mRender = new Render(baseGame);
            baseGame.Components.Add(mRender = new Render(baseGame));
            baseGame.Services.AddService(typeof(Render), mRender);
            mRender.DrawOrder = 1;
            mLevel.DrawOrder = -1;
            //use this to switch level
            */

            baseGame.Exit();
        }

        /*
        public bool Contains(Point a, Point b, Point m)
        {
            double distance = (((b.Y - a.Y) * m.X) - ((b.X - a.X) * m.Y) + (b.X * a.Y) - (b.Y * a.X)) / Math.Sqrt(((b.Y - a.Y) ^ 2 + (b.X - a.X) ^ 2));
            if (distance >= 0)
            {
                return true;
            }
            return false;
        }
        */
    }
}
