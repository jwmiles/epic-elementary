using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Controller : Microsoft.Xna.Framework.GameComponent
    {

        KeyboardState kbd;
        Player player;
        Window window;
        Projectiles proj;
        int lr, ud;
        float timer = 0;
        SoundEffect toss_sound;

        public Controller(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            player = Game.Services.GetService(typeof(Player)) as Player;
            window = Game.Services.GetService(typeof(Window)) as Window;
            proj = Game.Services.GetService(typeof(Projectiles)) as Projectiles;
            lr = ud = 0;

            toss_sound = Game.Content.Load<SoundEffect>(@"audio/sounds/toss");

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            MouseState ms = Mouse.GetState();
            if (ms.LeftButton == ButtonState.Pressed)
            {
                if (timer > 360)
                {

                    toss_sound.Play();
                    proj.toss(new Vector3(player.actor.position.X, player.actor.position.Y, player.actor.position.Z),
                        new Vector3(ms.X + window.location.X, player.actor.size.Y / 2, ms.Y),
                        true);
                    timer = 0;
                }
            }

            kbd = Keyboard.GetState();

            if (kbd.IsKeyDown(Keys.Space)) player.actor.jump();

           timer += gameTime.ElapsedGameTime.Milliseconds;


            if (!player.actor.inJump)
            {
                lr = horizontal(kbd);
                ud = vertical(kbd);
            }


            base.Update(gameTime);
        }

        private int horizontal(KeyboardState kbd)
        {

            if (kbd.IsKeyDown(Keys.D))
            {
                player.actor.move(1);
                player.actor.momentum[0] = 1;
                return 1;
            }
            else if (kbd.IsKeyDown(Keys.A))
            {
                player.actor.move(3);
                player.actor.momentum[0] = 3;
                return -1;
            }
            else
            {
                player.actor.momentum[0] = -1;
                return 0;
            }

        }

        private int vertical(KeyboardState kbd)
        {

            if (kbd.IsKeyDown(Keys.S))
            {
                player.actor.move(2);
                player.actor.momentum[1] = 2;
                return 1;
            }
            else if (kbd.IsKeyDown(Keys.W))
            {
                player.actor.move(0);
                player.actor.momentum[1] = 0;
                return -1;
            }
            else
            {
                player.actor.momentum[1] = -1;
                return 0;
            }

        }

    }
}
