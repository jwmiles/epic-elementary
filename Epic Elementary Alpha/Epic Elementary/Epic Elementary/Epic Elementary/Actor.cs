using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Actor
    {

        public Vector3 position;
        public int health;
        public int speed;
        public Vector3 size;
        public Texture2D spriteTexture;
        public int[] momentum = new int [2];
        public bool inJump;
        public Rectangle platform;
        public int jumpTimer;
        public float timer = 0f;
        int offset = 20;
        public float maxRange;
        public float minRange;
        float boost = 12f, jumpVelocity = 0, gravity = 1;
        public BoundingBox boundingbox;

        public Actor()
        {
            health = 100;
            momentum[0] = -1;
            momentum[1] = -1;
            inJump = false;
        }

        public Vector2 displayCoordinates()
        {
            return new Vector2(this.position.X, (this.position.Z + this.position.Y));
        }

        /* Sprite directions:

         *0: up
         *1: right
         *2: down
         *3: left
        
       */

        public void move(int dir)
        {
            switch (dir)
            {
                case 0:
                    this.position.Z -= this.speed;
                    if (this.position.Z + (this.size.Y - offset) < platform.Top) this.position.Z = platform.Top - (this.size.Y - offset);
                    break;
                case 1:
                    this.position.X += this.speed;
                    if (this.position.X > platform.Right) this.position.X = platform.Right;
                    break;
                case 2:
                    this.position.Z += this.speed;
                    if (this.position.Z + (this.size.Y + offset) > platform.Bottom) this.position.Z = platform.Bottom - (this.size.Y + offset);
                    break;
                case 3:
                    this.position.X -= this.speed;
                    if (this.position.X < platform.Left) this.position.X = platform.Left;
                    break;
            }
            adjustboundingbox();

        }

        public void jump(float time = 0f)
        {
            if (!inJump) jumpVelocity += boost;
            inJump = true;
            jumpVelocity -= 1;
            this.position.Y -= jumpVelocity;

            this.move(momentum[0]);
            this.move(momentum[1]);

            if (this.position.Y == 0)
            {
                this.position.Y = 0;
                inJump = false;

            }
            adjustboundingbox();
        }

        public void adjustboundingbox()
        {
            this.boundingbox = new BoundingBox(new Vector3(this.position.X, this.position.Y, this.position.Z), new Vector3(this.position.X + this.size.X, this.position.Y + this.size.Y, this.position.Z + this.size.Z));
        }

    }
}
