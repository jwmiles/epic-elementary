﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Epic_Elementary
{
    public class Animation
    {
        public int spriteWidth;
        public int spriteHeight;
        public int spriteDepth;
        public float scale;
        public float time = 0;
        public int[] momentum;
        Object obj;
        public int interval = 80;
        public int frame = 0, animation_length;
        public bool flipped = false;
        State state;

        enum State
        {
            //Up
            Up,
            //Down
            Down,
            //Side
            Side,
            //Walking
            Walking
        }

        public Animation(Object ob)
        {
            momentum = new int[] { -1, -1 };
            if (ob is Player)
            {
                obj = (Player)ob;
                spriteWidth = 32;
                spriteHeight = 31;
            }
            else
            {
                obj = ob;
                spriteHeight = 31;
                spriteWidth = 32;
                spriteDepth = 20;
                scale = 4;
            }
        }


        public Rectangle animation(int[] movement, GameTime gameTime)
        {
            if (movement[0] == this.momentum[0] && movement[1] == this.momentum[1])
            {
                this.time += gameTime.ElapsedGameTime.Milliseconds;
            } 
            else
            {
                this.time = 0;
                if (this.state == State.Walking)
                {
                    this.state = State.Side;
                }
                this.momentum = movement;
            }
            return _animatePlayer();
        }

        private Rectangle _animatePlayer()
        {
            if (this.momentum[0] == -1 && this.momentum[1] == -1 && this.state == State.Walking) this.state = State.Side;
            if (this.momentum[0] == 1) { this.state = State.Walking; this.flipped = true; }
            else if (this.momentum[0] == 3) { this.state = State.Walking; this.flipped = false; }
            else
            {
                if (this.momentum[1] == 2) this.state = State.Down;
                else if (this.momentum[1] == 0) this.state = State.Up;
            }

            switch (state)
            {
                case State.Up:
                    this.frame = 6;
                    this.time = 0;
                    break;
                case State.Down:
                    this.frame = 0;
                    this.time = 0;
                    break;
                case State.Side:
                    this.frame = 1;
                    this.time = 0;
                    break;
                case State.Walking:
                    if (this.time > this.interval * (this.frame))
                    {
                        this.frame = 1 + this.frame + 1;
                        if (this.frame >= 6) { this.frame = 1; this.time = 0; }
                    }
                    break;
                default:
                    this.frame = 0;
                    this.time = 0;
                    break;
            }
            
            return new Rectangle(spriteWidth*this.frame,0,spriteWidth,spriteHeight);

        }
    }
}
