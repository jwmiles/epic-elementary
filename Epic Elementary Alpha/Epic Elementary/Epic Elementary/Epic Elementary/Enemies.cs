using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary 
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    /// 

    public class Enemies : Microsoft.Xna.Framework.GameComponent
    {  

        public Enemies(Game game)
            : base(game)
        {
            // TODO: Construct any child components here


        }

        public List<Actor> enemies = new List<Actor>();
        public List<Animation> animations = new List<Animation>();

        Level level;
        Window window;
        Actor actor;
        Animation animate;
        Rectangle frame;

        float minOffset;
        public float findRadius, fireRadius, retreatRadius;
        public int count;
        Random rnd = new Random();

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {

            level = Game.Services.GetService(typeof(Level)) as Level;
            window = Game.Services.GetService(typeof(Window)) as Window;
            
            count = ((level.id * 3) + (rnd.Next(1, 10)));

            for (int i = 0; i < count; i++)
            {

                actor = new Actor();
                animate = new Animation(this);

                animate.scale = (level.r_platform.Height / 4f) / animate.spriteHeight;
                actor.platform = level.r_platform;
                actor.speed = (int)5.0;
                actor.size = new Vector3(animate.spriteWidth * animate.scale, animate.spriteHeight * animate.scale, animate.spriteDepth * animate.scale);

                findRadius = actor.size.X * 6;
                fireRadius = actor.size.X * 4;
                retreatRadius = actor.size.X * 2;
                minOffset = findRadius * 2;
                actor.position = new Vector3((float)rnd.Next((int)minOffset, level.r_platform.Right), 0, (float)rnd.Next(level.r_platform.Top, (int)(level.r_platform.Bottom-actor.size.Y)));
                
                actor.spriteTexture = Game.Content.Load<Texture2D>(@"images/enemies/goalie");
                actor.adjustboundingbox();
                enemies.Add(actor);
                animations.Add(animate);

            }

            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            
        }
        /*
        public override void Draw(GameTime gameTime)
        {
            batch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;

            base.Draw(gameTime);

            batch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, null, Matrix.CreateTranslation(window.location * -1));

            for (int i = 0; i < count; i++)
            {
                batch.Draw(
                    enemies[i].spriteTexture, 
                    enemies[i].displayCoordinates(), 
                    animations[i].animation(),
                    Color.White,
                    0f,
                    new Vector2(0f, 0f),
                    animations[i].scale,
                    SpriteEffects.None,
                    0);
            }

            batch.End();

            base.Update(gameTime);
        }
         */
    }
}
