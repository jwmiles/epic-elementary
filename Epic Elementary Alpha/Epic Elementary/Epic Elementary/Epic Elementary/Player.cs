using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Player : Microsoft.Xna.Framework.GameComponent
    {

        public Actor actor;
        public Animation animate;
        Level level;
        Window window;
        SpriteBatch batch;
        Projectiles abc;

        public Rectangle frame;

        public Player(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            abc = new Projectiles(game);
        }
        
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            window = Game.Services.GetService(typeof(Window)) as Window;
            level = Game.Services.GetService(typeof(Level)) as Level;

            actor = new Actor();
            animate = new Animation(this);

            animate.scale = (level.r_platform.Height / 4f) / animate.spriteHeight;
            actor.platform = level.r_platform;
            actor.speed = (int)7.5;
            actor.size = new Vector3(animate.spriteWidth * animate.scale, animate.spriteHeight * animate.scale, animate.spriteDepth * animate.scale);
            actor.position = new Vector3(0, 0, (level.r_platform.Top + level.r_platform.Bottom) / 2);
            actor.minRange = actor.size.X;
            actor.maxRange = actor.size.X * 6;

            base.Initialize();

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            frame = animate.animation(this.actor.momentum, gameTime);

            if (actor.inJump)
            {
                actor.jump((float)gameTime.ElapsedGameTime.TotalMilliseconds);
            }

            base.Update(gameTime);

        }

        /*
        public override void Draw(GameTime gameTime)
        {
            batch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;

            base.Draw(gameTime);

            batch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, null, Matrix.CreateTranslation(window.location * -1));

            batch.Draw(actor.spriteTexture, actor.displayCoordinates(), frame, Color.White, 0f, new Vector2(0f,0f), animate.scale, SpriteEffects.None, 0);

            batch.End();
        }
        */

    }
}
