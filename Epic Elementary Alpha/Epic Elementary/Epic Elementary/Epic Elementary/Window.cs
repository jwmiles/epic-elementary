using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Window : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Vector2 view;
        public Vector3 location;

        Player player;
        Level level;

        Game bas;

        public Window(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            bas = game;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            //view = new Vector2(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height);
           
            player = Game.Services.GetService(typeof(Player)) as Player;
            level = Game.Services.GetService(typeof(Level)) as Level;
            location = new Vector3(0, 0, 0);

            base.Initialize(); 
            
            view = new Vector2(GraphicsDevice.PresentationParameters.BackBufferWidth, GraphicsDevice.PresentationParameters.BackBufferHeight);
            
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {

            if (player.actor.position.X >= (view.X / 2))
            {
                location.X = player.actor.position.X - (view.X / 2);
            }
            if ((location.X) < level.r_platform.Left) location.X = 0;
            if ((location.X + view.X) > level.r_platform.Right) location.X = level.r_platform.Right - view.X;


            // TODO: Add your update code here
            base.Update(gameTime);
        }
    }
}
