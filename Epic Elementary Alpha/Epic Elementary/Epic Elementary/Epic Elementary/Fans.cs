using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Fans : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Game baseGame;
        public Vector2 position;
        public Texture2D[] frames;
        public int frame;
        public int delay;
        public ContentManager Content;
        public SpriteBatch spriteBatch;
        
        public Fans(Game game, Vector2 pos)
            : base(game)
        {
            baseGame = game;
            this.position = pos;
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            frames = new Texture2D[10];
            frame = 0;
            delay = 0;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            frames[0] = baseGame.Content.Load<Texture2D>("images/Fan0");
            frames[1] = baseGame.Content.Load<Texture2D>("images/Fan1");
            frames[2] = baseGame.Content.Load<Texture2D>("images/Fan2");
            frames[3] = baseGame.Content.Load<Texture2D>("images/Fan3");
            frames[4] = baseGame.Content.Load<Texture2D>("images/Fan4");
            frames[5] = baseGame.Content.Load<Texture2D>("images/Fan5");
            frames[6] = baseGame.Content.Load<Texture2D>("images/Fan6");
            frames[7] = baseGame.Content.Load<Texture2D>("images/Fan7");
            frames[8] = baseGame.Content.Load<Texture2D>("images/Fan8");
            frames[9] = baseGame.Content.Load<Texture2D>("images/Fan9");
            
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            frame = (int) Math.Floor((delay % 20.0)/2.0);

            delay++;
            if (delay > 20)
                delay = 0;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            spriteBatch = baseGame.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(frames[frame], position, Color.White);
            spriteBatch.End();
        }
    }
}
