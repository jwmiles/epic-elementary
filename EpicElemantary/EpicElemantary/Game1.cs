using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace EpicElemantary
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        Texture2D ground;
        Rectangle groundR;
        Vector2 groundPos;
        SpriteBatch spriteBatch;
        AnimatedSprite sprite;        
        int width;
        int height;
        protected Song song;
        SoundEffect[] throwSounds = new SoundEffect[5];
        KeyboardState ckb;
        KeyboardState pkb;
        
       

        public Game1()
        {

            
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            width = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            height = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
            groundPos = new Vector2(0, 500);
            groundR = new Rectangle(0, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height/3);
            
            
        }

       





        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            
            ground = Content.Load<Texture2D>(@"Image/grass");
            song = Content.Load<Song>(@"audio/background");
            MediaPlayer.Play(song);
            sprite = new AnimatedSprite(Content.Load<Texture2D>(@"Image/Jack3"), 1, 32, 33);
            sprite.Position = new Vector2(50, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 3 + 100);
            throwSounds[0] = Content.Load<SoundEffect>(@"audio/Throw");


            
           


            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            sprite.HandleSpriteMovement(gameTime);
            // Allows the game to exit

            ckb = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();


            
            if (ckb.IsKeyDown(Keys.Space) && pkb.IsKeyUp(Keys.Space))
            {

                throwSounds[0].Play(1.0f, 0f, 0f);
               
            }
           

            base.Update(gameTime);

            pkb = ckb;
            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            spriteBatch.Draw(ground, groundR, Color.White);
            spriteBatch.Draw(sprite.Texture, sprite.Position, sprite.SourceRect, Color.White, 0f, sprite.Origin, 4.0f, SpriteEffects.None,0);
            spriteBatch.End();


            

            base.Draw(gameTime);


        }
    }
}
