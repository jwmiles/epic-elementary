﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EpicElemantary
{
    class AnimatedSprite
    {

        KeyboardState currentKBState;
        KeyboardState previousKBState;
        Texture2D spriteTexture;
        float timer = 0f;
        float interval = 200f;
        int currentFrame = 0;
        int spriteWidth = 32;
        int spriteHeight = 33;
        int spriteSpeed = 5000;
        Rectangle sourceRect;
        Vector2 position;
        Vector2 origin;





        public Vector2 Position
        {
            get { return position; }

            set { position = value; }
        }


        public Vector2 Origin
        {
            get { return origin; }

            set { origin = value; }
        }

        public Texture2D Texture
        {
            get { return spriteTexture; }

            set { spriteTexture = value; }
        }

        public Rectangle SourceRect
        {
            get { return sourceRect; }

            set { sourceRect = value; }
        }


        public AnimatedSprite(Texture2D texture, int currentFrame, int spriteWidth, int spriteHeight)
        {

            this.spriteTexture = texture;
            this.currentFrame = currentFrame;
            this.spriteWidth = spriteWidth;
            this.spriteHeight = spriteHeight;
        }

        public void HandleSpriteMovement(GameTime gameTime)
        {
            previousKBState = currentKBState;
            currentKBState = Keyboard.GetState();
            sourceRect = new Rectangle(currentFrame * spriteWidth, 0, spriteWidth, spriteHeight);

            if (currentKBState.GetPressedKeys().Length == 0)  //If statement to make a default standing pose when no buttons are pressed
            {
             /*   if (currentFrame == 0)
                {
                    currentFrame = 0;
                }
*/
                if (currentFrame > 1 && currentFrame < 5)
                {

                    currentFrame = 1;
                }

              /*  if (currentFrame == 6)
                {

                    currentFrame = 6;

                }
                */
                if (currentFrame > 7 && currentFrame < 11)
                {

                    currentFrame = 7;
                }

            }// end if for nothing pressed

           if (currentKBState.IsKeyDown(Keys.Tab))  {
                spriteSpeed = 3;
                interval = 100;
           }
            else {
                spriteSpeed = 2;
                interval = 200;
           }

            
            if (currentKBState.IsKeyDown(Keys.Right) == true)  {

                AnimateRight(gameTime);

                if (position.X < GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - 25)
                {
                    position.X += spriteSpeed;
                }
            }

            if (currentKBState.IsKeyDown(Keys.Left) == true)
            {
                AnimateLeft(gameTime);
                if (position.X > GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width + 25)
                {

                    position.X -= spriteSpeed;

                }
            }


            if (currentKBState.IsKeyDown(Keys.Up) == true)
            {
                AnimateUp(gameTime);
                if (position.Y > GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2 - 50)
                {

                    position.Y -= spriteSpeed;

                }
            }

            if (currentKBState.IsKeyDown(Keys.Down) == true)
            {
                AnimateDown(gameTime);
                if (position.Y < GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height/3 + 60)
                {

                    position.Y += spriteSpeed;

                }
            }


            origin = new Vector2(sourceRect.Width / 2, sourceRect.Height / 2);


        }



        public void AnimateLeft(GameTime gameTime)
        {
            if (currentKBState != previousKBState)
            {
                currentFrame = 2;
            }

            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (timer > interval)
            {
                currentFrame++;

                if (currentFrame > 5)
                {
                    currentFrame = 1;
                }
                timer = 0f;
            }

        }


        public void AnimateRight(GameTime gameTime)
        {
            if (currentKBState != previousKBState)
            {
                currentFrame = 8;
            }

            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (timer > interval)
            {
                currentFrame++;

                if (currentFrame > 11)
                {
                    currentFrame = 8;
                }
                timer = 0f;
            }

        }


        public void AnimateUp(GameTime gameTime)
        {
            if (currentKBState != previousKBState)
            {
                currentFrame = 6;
            }

            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (timer > interval)
            {
                currentFrame++;

                if (currentFrame > 6)
                {
                    currentFrame = 6;
                }
                timer = 0f;
            }

        }


        public void AnimateDown(GameTime gameTime)
        {
            if (currentKBState != previousKBState)
            {
                currentFrame = 0;
            }

            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (timer > interval)
            {
                currentFrame++;

                if (currentFrame > 0)
                {
                    currentFrame = 0;
                }
                timer = 0f;
            }

        }





       


    }

}

