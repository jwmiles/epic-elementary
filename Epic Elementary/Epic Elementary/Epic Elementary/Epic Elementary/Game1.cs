using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Epic_Elementary
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
       
        State state;

        int width, height;

        //public GameComponent mActor;
        public GameComponent mLevel;
        public GameComponent mWindow;
        public GameComponent mController;
        public GameComponent mPlayer;
        public GameComponent mEnemies;
        public GameComponent mAI;
        public GameComponent mRender;

        enum State
        {
            title,
            map,
            game,
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            state = State.game;


            switch (state)
            {
                case State.map:
                    break;
                case State.game:
                    GameComponent mController = new Controller(this);
                    this.Components.Add(mController = new Controller(this));
                    this.Services.AddService(typeof(Controller), mController);
                    

                    DrawableGameComponent mWindow = new Window(this);
                    //GameComponent mActor = new Actor(this);
                    DrawableGameComponent mLevel = new Level(this);
                    this.Components.Add(mWindow = new Window(this));
                    this.Services.AddService(typeof(Window), mWindow);
                    //this.Components.Add(mActor = new Actor(this));
                    this.Components.Add(mLevel = new Level(this));
                    //this.Services.AddService(typeof(Actor), mActor);
                    this.Services.AddService(typeof(Level), mLevel);
                    
                    GameComponent mPlayer = new Player(this);
                    this.Components.Add(mPlayer = new Player(this));
                    this.Services.AddService(typeof(Player), mPlayer);
                    
                    GameComponent mEnemies = new Enemies(this);
                    this.Components.Add(mController = new Enemies(this));
                    this.Services.AddService(typeof(Enemies), mController);

                    GameComponent mAI = new AI(this);
                    this.Components.Add(mController = new AI(this));
                    this.Services.AddService(typeof(AI), mController);
                    
                    DrawableGameComponent mRender = new Render(this);
                    this.Components.Add(mController = new Render(this));
                    this.Services.AddService(typeof(Render), mController);

                    mRender.DrawOrder = 1;
                    mLevel.DrawOrder = -1;
                    break;
                case State.title:
                default:

                    break;
            }
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            this.IsMouseVisible = true;
            //graphics.IsFullScreen = true;
            width = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            height = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferWidth = width/2;
            graphics.PreferredBackBufferHeight = height/2;
            graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.

            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.Services.AddService(typeof(SpriteBatch), spriteBatch);

            

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();


            // TODO: Add your update logic here


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
