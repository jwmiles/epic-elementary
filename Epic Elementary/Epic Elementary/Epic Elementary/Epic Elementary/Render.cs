using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Render : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Render(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        Enemies enemy;

        Player player;

        Projectiles projectile;

        SpriteBatch batch;
        Window window;

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            window = Game.Services.GetService(typeof(Window)) as Window;
            player = Game.Services.GetService(typeof(Player)) as Player;
            enemy = Game.Services.GetService(typeof(Enemies)) as Enemies;

            base.Initialize();
        }

        protected override void LoadContent()
        {

            player.actor.spriteTexture = Game.Content.Load<Texture2D>(@"images/player/jack");

            base.LoadContent();

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }



        public override void Draw(GameTime gameTime)
        {
            batch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;

            base.Draw(gameTime);

            batch.Begin(SpriteSortMode.BackToFront, null, SamplerState.LinearWrap, null, null, null, Matrix.CreateTranslation(window.location * -1));

            batch.Draw(
                player.actor.spriteTexture,
                player.actor.displayCoordinates(),
                player.frame, 
                Color.White, 
                0f, 
                new Vector2(0f,0f), 
                player.animate.scale, 
                SpriteEffects.None,
                (1 / player.actor.position.Z)
            );

            for (int i = 0; i < enemy.count; i++)
            {
                batch.Draw(
                    enemy.enemies[i].spriteTexture,
                    enemy.enemies[i].displayCoordinates(),
                    enemy.animations[i].animation(),
                    Color.White,
                    0f,
                    new Vector2(0f, 0f),
                    enemy.animations[i].scale,
                    SpriteEffects.None,
                    (1/enemy.enemies[i].position.Z));
            }

            batch.End();

            base.Update(gameTime);
        }
    }
}
