﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Epic_Elementary
{
    public class Animation
    {
        public int spriteWidth;
        public int spriteHeight;
        public int spriteDepth;
        public float scale;
        Object obj;

        public Animation(Object ob)
        {
            if (ob is Player)
            {
                obj = ob;
                spriteWidth = 32;
                spriteHeight = 31;
                spriteDepth = 20;
            }
            else
            {
                obj = ob;
                spriteHeight = 31;
                spriteWidth = 32;
                spriteDepth = 20;
                scale = 4;
            }
        }

        public Rectangle animation()
        {
            return _animatePlayer();
        }

        private Rectangle _animatePlayer()
        {
            
            return new Rectangle(0,0,spriteWidth,spriteHeight);

        }


    }
}
