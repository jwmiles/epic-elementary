using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Projectiles : Microsoft.Xna.Framework.GameComponent
    {
        List<Projectile> projectiles = new List<Projectile>();
        float time = 0f;

        public Projectiles(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        public void toss(Vector3 origin, Vector3 destination)
        {
            projectiles.Add(new Projectile(origin, destination));
        }

        public void to() { }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            for (int i = 0; i < projectiles.Count; i++)
            {
                if (projectiles[i].active == true)
                {
                    if (projectiles[i].position.Y <= 0)
                    {
                        projectiles[i].position.Y = 0;
                        projectiles[i].active = false;
                        projectiles[i].timeout = time;
                    }
                    else
                    {
                        projectiles[i].move();
                    }
                }
                else
                {

                }
            }

            base.Update(gameTime);
        }
    }
}
