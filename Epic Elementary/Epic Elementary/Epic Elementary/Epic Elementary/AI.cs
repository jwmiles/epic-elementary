using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AI : Microsoft.Xna.Framework.GameComponent
    {
        public AI(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        Enemies enemy;
        List<Actor> enemies;

        Player player;

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            enemy = Game.Services.GetService(typeof(Enemies)) as Enemies;
            enemies = enemy.enemies;

            player = Game.Services.GetService(typeof(Player)) as Player;

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            for (int i = 0; i < enemies.Count; i++)
            {
                double distance = Math.Sqrt(Math.Pow((enemies[i].position.X - player.actor.position.X), 2) + Math.Pow((enemies[i].position.Z - player.actor.position.Z), 2));
                if (distance < enemy.findRadius && distance > enemy.fireRadius) {
                    if (enemies[i].position.X > player.actor.position.X)
                    {
                        enemies[i].move(3);
                    }
                    else if (enemies[i].position.X < player.actor.position.X)
                    {
                        enemies[i].move(1);
                    }
                    if (enemies[i].position.Z > player.actor.position.Z)
                    {
                        enemies[i].move(0);
                    }
                    else if (enemies[i].position.Z < player.actor.position.Z)
                    {
                        enemies[i].move(2);
                    }
                }
            }

            base.Update(gameTime);
        }
    }
}
