using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Epic_Elementary
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Level : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch levelBatch;
        Texture2D platform, ground, sky, backdrop;
        Window window;
        Vector2 dimensions, origin;
        public Game baseGame;

        public Rectangle r_platform, r_ground, r_sky, r_backdrop;
        Rectangle p_tiler, g_tiler, s_tiler, b_tiler;

        public int id = 1;

        public Level(Game game)
            : base(game)
        {
            baseGame = game;
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            //spriteBatch = baseGame.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;
            window = Game.Services.GetService(typeof(Window)) as Window;

            origin = new Vector2(0, (window.view.Y * 3.5f) / 6);
            dimensions = new Vector2(window.view.X * 2, (window.view.Y * 2) / 6);

            r_platform = new Rectangle((int)origin.X, (int)origin.Y, (int)dimensions.X, (int)dimensions.Y);
            r_sky = new Rectangle(0, 0, (int)window.view.X, r_platform.Top);
            r_ground = new Rectangle((int)origin.X, r_platform.Bottom, (int)dimensions.X, (int)window.view.Y);


            base.Initialize();
        }

        protected override void LoadContent()
        {

            //levelBatch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;

            platform = baseGame.Content.Load<Texture2D>(@"images/grass");
            ground = Game.Content.Load<Texture2D>(@"images/dirt");
            sky = Game.Content.Load<Texture2D>(@"images/sky");
            Song song = Game.Content.Load<Song>(@"audio/music/background");

            //MediaPlayer.Play(song);

            p_tiler = new Rectangle(0, 0, platform.Width*((int)dimensions.X / platform.Width), platform.Height*((int)dimensions.Y / platform.Height));
            g_tiler = new Rectangle(0, 0, ground.Width * ((int)dimensions.X / ground.Width), ground.Height * ((int)dimensions.Y / ground.Height));
            s_tiler = new Rectangle(0, 0, sky.Width * ((int)window.view.X / sky.Width) / 4, sky.Height);

            base.LoadContent();

        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            levelBatch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;

            base.Draw(gameTime);

            // Slow sliding
            levelBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
                levelBatch.Draw(sky, r_sky, s_tiler, Color.White);
            levelBatch.End();

            // Normal Sliding
            levelBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, null, Matrix.CreateTranslation(window.location * -1));
                levelBatch.Draw(ground, r_ground, g_tiler, Color.White);
                levelBatch.Draw(platform, r_platform, g_tiler, Color.White);
            levelBatch.End();
        }
    }
}
