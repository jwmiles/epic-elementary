﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Epic_Elementary
{
    class Projectile
    {
        public Vector3 position, size, origin, destination;
        public Texture2D texture;
        public bool isPlayer, active;
        public float timeout;
        public int vVelocity;

        public Projectile(Vector3 origin, Vector3 dest)
        {
            size = new Vector3(20, 20, 20);
            active = true;
            isPlayer = false;
            send(origin, dest);
        }

        public void send(Vector3 origin, Vector3 destination)
        {
            this.origin = origin;
            this.destination = destination;
        }

        public void move()
        {
            vVelocity -= -1;
            this.position.Y -= vVelocity;

            this.position.X += (this.destination.X - this.origin.X) / 12;
            this.position.Z += (this.destination.Z - this.origin.Z) / 12;
        }
    }
}
